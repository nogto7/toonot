<?php $this->js('map'); ?>
<div class="search_container">
    <div class="search_layout">
        <div class="filter_wrap">
            <ul class="filter_list dfc">
                <li>
                    <div class="real_form_content dfc">
                        <div class="real_form_items dfc">
                            <div class="rf_item">
                                <select class="select">
                                    <option value="1">Орон сууц</option>
                                    <option value="2">Хашаа байшин</option>
                                    <option value="3">Газар</option>
                                    <option value="4">Зуслан</option>
                                </select>
                            </div>
                            <div class="rf_item">
                                <div class="rf_list">
                                    <div><h4>1</h4><span>өрөө</span></div>
                                    <div><h4>2</h4><span>өрөө</span></div>
                                    <div><h4>3</h4><span>өрөө</span></div>
                                    <div><h4>4</h4><span>өрөө</span></div>
                                </div>
                            </div>
                            <div class="rf_item">
                                
                            </div>
                        </div>
                        <button class="btn btn_l default __ml1">Хайлтыг хадгалах</button>
                    </div>
                </li>
            </ul>
        </div>
        <div class="filter_result">
            <div class="dg g2 gap1_6">
                <div class="result_wrap">
                    <h2>Үл хөдлөх хөрөнгө зарна</h2>
                    <div class="dg g2">
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div><div class="__image_box_content"><img class="" src="images/project_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">130.5 м<sup>2</sup> 4 өрөө байр</h2>
                                <div class="__prod_price">320,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img1.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">170.5 м<sup>2</sup> 5 өрөө байр</h2>
                                <div class="__prod_price">400,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img2.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                                <div class="__prod_price">280,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content"><img class="" src="images/project_img3.jpeg" /></div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">130.5 м<sup>2</sup> өрөө байр</h2>
                                <div class="__prod_price">320,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img2.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                                <div class="__prod_price">280,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img1.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                                <div class="__prod_price">280,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div><div class="__image_box_content"><img class="" src="images/project_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">130.5 м<sup>2</sup> 4 өрөө байр</h2>
                                <div class="__prod_price">320,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img1.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">170.5 м<sup>2</sup> 5 өрөө байр</h2>
                                <div class="__prod_price">400,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img2.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                                <div class="__prod_price">280,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content"><img class="" src="images/project_img3.jpeg" /></div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">130.5 м<sup>2</sup> өрөө байр</h2>
                                <div class="__prod_price">320,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img2.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                                <div class="__prod_price">280,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                        <div class="box_item prod_box box_vertical">
                            <div class="prod_header">
                                <div class="__image_box">
                                    <div class="__image_box_content">
                                    <img class="" src="images/project_img1.jpeg" />
                                    </div>
                                    <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                                </div>
                            </div>
                            <div class="prod_main">
                                <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                                <div class="__prod_price">280,000,000₮</div>
                                <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="gmap" class="map_wrap"></div>
                </div>
            </div>
        </div>
    </div>
</div>
