<?
$CJSv=1;
$suf=$CJSv?'?'.$CJSv:'';
?>
<!doctype html>
<html><head>
	<meta charset="utf-8" />
	<base href="<?=$this->base?>" />
	<title><?=$this->tit()?:'The bePRO - Үл хөдлөх хөрөнгийн платформ'?></title>
	<link href="images/favicon.png" rel="shortcut icon" type="image/png" />	
<?
foreach($this->meta() as $k=>$v) echo '
	<meta name="'.str_replace('"','&quot;',$k).'" content="'.str_replace('"','&quot;',$v).'" />';?>
	<link rel="stylesheet" type="text/css" href="css/nice-select.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNt1bOU5k0JzhC3SBqRXifmShO-4qkP2Y&callback=initMap"></script>
<?
foreach($this->css() as $k=>$v){
	$isExternal=strpos($v,'://')||substr($v,0,2)=='//';
	echo '
	<link rel="stylesheet" href="'.($isExternal?$v:'css/'.$v.'.css').'" tyle="text/css" />';
}
foreach($this->js(true) as $k=>$v){
	$isExternal=strpos($v,'://')||substr($v,0,2)=='//';
	echo '
	<script type="text/javascript" src="'.($isExternal?$v:'js/'.$v.'.js'.$suf).'"></script>';
}
?>
</head><body>
<?if(isset($_COOKIE['viewer'])) echo $html;
else require_once PAGE.'login.php';?>
<script type="text/javascript" src="js/nice-select.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<?
foreach($this->js(false) as $k=>$v){
	$isExternal=strpos($v,'://')||substr($v,0,2)=='//';
	echo '
	<script type="text/javascript" src="'.($isExternal?$v:'js/'.$v.'.js'.$suf).'"></script>';
}?>
</body></html>