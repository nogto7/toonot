<?php
$p=dirname(dirname(PAGE)).DS.'cache'.DS.'pull.txt';
if(isset($_POST['msg'])){
	$h=fopen($p,'w+');
	fwrite($h,$_POST['msg']);
	fclose($h);
	die();
}
$dtti=date('Y-m-d H:i');
$h=fopen($p,'w+');
fwrite($h,$dtti);
fclose($h);
?>
<style type="text/css">
.puller{position:fixed;top:0;bottom:0;left:0;right:0;background:rgba(0,0,0,0.8);z-index:10;display:flex;justify-content:center;align-items:center;}
.puller>div{background:#fff;width:500px;border-radius:10px;padding:20px;font-size:40px;text-align:center;}
.puller>div>b{font-size:100px;display:block;}
.puller>div>b.done{color:green;}
.puller>div>h3{font-size:20px;text-align:left;}
.puller>div>textarea{width:100%;box-sizing:border-box;border:1px solid #ddd;border-rasius:5px;padding:5px 10px;}
</style>
<div class="puller"><div>
    <b id="wait">1</b>секунд хүлээнэ үү
    <hr />
    <h3>Commit message</h3>
    <textarea onchange="$.post(location.href,{msg:this.value})"><?=$dtti?></textarea>
</div></div>
<script type="text/javascript">
    var n=<?=60-(time()%60)?>;
    function sec(){$('#wait').text(--n)}
    sec(),setInterval(function(){if(n) sec();else{$('.puller>div').html('<b class="done">git pull</b>command done'),setTimeout(function(){$('.puller').fadeOut(500,function(){$(this).remove()})},1e4)}},1e3)
</script>