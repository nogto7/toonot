<main class="main_wrap">
    <div class="main_block">
        <div class="container">
            <div class="real_view">
                <div class="dg g2 gap2_4">
                    <div class="dg g2 gap2_4">
                        <a href="list" class="real_tab btn_htrans">
                            <div class="dssb">
                                <div class="tab_icon sell"></div>
                                <div class="rv_next __ml2 btn_htrans dfcc">
                                    <span class="icon next_arrow"></span>
                                </div>
                            </div>
                            <div class=" dsb aic">
                                <div class="flex_g dsb aic">
                                    <div class="ha_big">Зарна</div>
                                    <h2>13632</h2>
                                </div>
                            </div>
                        </a>
                        <a href="list" class="real_tab btn_htrans">
                            <div class="dssb">
                                <div class="tab_icon rent">
                                </div>
                                <div class="rv_next __ml2 btn_htrans dfcc">
                                    <span class="icon next_arrow"></span>
                                </div>
                            </div>
                            <div class="dsb aic">
                                <div class="flex_g dsb aic">
                                    <div class="ha_big">Түрээслүүлнэ</div>
                                    <h2>5463</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="real_form">
                        <form>
                            <label class="__label">Хүссэн <strong>PRO</strong>-гоо хай</label>
                            <div class="real_form_content dfc">
                                <div class="real_form_items dfc">
                                    <div class="rf_item">
                                        <input type="text" class="input input_l" placeholder="19,095 зар байна" />
                                    </div>
                                    <div class="rf_item __rf_region">
                                        <select class="select">
                                            <option value="1">Бүх байршил</option>
                                            <option value="1">Баянгол</option>
                                            <option value="1">Баянзүрх</option>
                                            <option value="1">Хан-Уул</option>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn_l primary search_btn __ml1"><span class="search"></span>Хайх</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="reclam home"><span>Сурталчилгаа</span>
                    <div class="reclam_img"><img src="images/main_img1.jpeg" /></div>
                </div>
            </div>
            <div class="section">
                <h2 class="section_title dcsp">Онцолсон төсөл
                    <div class="dfc prod_list_arrow"><div class="arrow_btn dfcc disabled"><span class="icon prev"></span></div><div class="arrow_btn dfcc active"><span class="icon next"></span></div></div></h2>
                <div class="pl dg g3">
                    <div class="box_item prod_box box_vertical proj">
                        <a href="project/river-castle" class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/d1.jpeg" />
                                </div>
                                <div class="__image_box_content">
                                <img class="" src="images/d2.jpg" />
                                </div>
                            </div>
                        </a>
                        <div class="prod_main">
                            <div class="dcsp">
                                <a href="project/river-castle" class="__prod_title">River Castle</a>
                                <div class="develop_logo"><img src="images/0-1.jpeg" /></div>
                            </div>
                            <div class="__prod_proj_desc">Монголд анх удаа 26 давхар орон сууц үйлчилгээний зориулалттай River castle бизнес зэрэглэлийн орон сууц</div>
                            <p class="__prod_location">Улаанбаатар, Сүхбаатар</p>
                            <div class="__project_room_list">
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>1 өрөө</p><p>41.5<sup>м2</sup></p><p>186,750,000₮</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>2 өрөө</p><p>53.5-66.5<sup>м2</sup></p><p>240.0-300.0₮ сая</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>3 өрөө</p><p>78.0-114.0<sup>м2</sup></p><p>351.0-513.0₮ сая</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>4 өрөө</p><p>125.0-171.8<sup>м2</sup></p><p>563.0-771.0₮ сая</p>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="__prod_more">
                            <button class="btn btn_l primary __w10">Дэлгэрэнгүй</button>
                        </div> -->
                    </div>
                    <div class="box_item prod_box box_vertical proj">
                        <div class="prod_header">
                            <a href="project/river-castle" class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/wy1.jpeg" />
                                </div>
                                <div class="__image_box_content">
                                <img class="" src="images/wy2.jpeg" />
                                </div>
                            </a>
                        </div>
                        <div class="prod_main">
                            <div class="dsb">
                                <a href="project/river-castle" class="__prod_title">Wales Yard Village</a>
                                <div class="develop_logo"><img src="images/wales_yard.jpeg" /></div>
                            </div>
                            <div class="__prod_proj_desc">Богд Хан уулын өвөрт үзэсгэлэнт жаргалантын аманд байрлалтай 44 айлын таун хаус, үйлчилгээний төв, фитнес, тоглоомын талбай бүхий хотхон.</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            <div class="__project_room_list">
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>1 өрөө</p><p>28.9 м<sup>2</sup>-с</p><p>78,000,000₮ -с</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>2 өрөө</p><p>54.25 м<sup>2</sup>-с</p><p>110,000,000₮ -с</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>3 өрөө</p><p>67.85 м<sup>2</sup>-с</p><p>140,000,000₮ -с</p>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="__prod_more">
                            <button class="btn btn_l primary __w10">Дэлгэрэнгүй</button>
                        </div> -->
                    </div>
                    <div class="box_item prod_box box_vertical proj">
                        <div class="prod_header">
                            <a href="project/river-castle" class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/project_img3.jpeg" />
                                </div>
                                <div class="__image_box_content">
                                <img class="" src="images/project_img1.jpeg" />
                                </div>
                            </a>
                        </div>
                        <div class="prod_main">
                            <div class="dsb">
                                <a href="project/river-castle" class="__prod_title">LCD "LesART"</a>
                                <div class="develop_logo"><img src="images/develop_logo.svg" /></div>
                            </div>
                            <div class="__prod_proj_desc">Нарийн өнгөлгөөтэй орон сууцнууд. SberMobile-аас ухаалаг гэр. хаалттай хашаа</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                            <div class="__project_room_list">
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>1 өрөө</p><p>28.9 м<sup>2</sup>-с</p><p>78,000,000₮ -с</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>2 өрөө</p><p>54.25 м<sup>2</sup>-с</p><p>110,000,000₮ -с</p>
                                </div>
                                <div class="__proj_list_item dg __g2_3_5 aic">
                                    <p>3 өрөө</p><p>67.85 м<sup>2</sup>-с</p><p>140,000,000₮ -с</p>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="__prod_more">
                            <button class="btn btn_l primary __w10">Дэлгэрэнгүй</button>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="section">
                <h2 class="section_title dsb">Шинэ зар</h2>
                <div class="pl dg g4">
                    <div class="box_item prod_box box_vertical">
                        <div class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div><div class="__image_box_content"><img class="" src="images/project_img1.jpeg" /></div>
                            </div>
                        </div>
                        <div class="prod_main">
                            <h2 class="__prod_title_mini">130.5 м<sup>2</sup> 4 өрөө байр</h2>
                            <div class="__prod_price">320,000,000₮</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                        </div>
                    </div>
                    <div class="box_item prod_box box_vertical">
                        <div class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/project_img1.jpeg" />
                                </div>
                                <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                            </div>
                        </div>
                        <div class="prod_main">
                            <h2 class="__prod_title_mini">170.5 м<sup>2</sup> 5 өрөө байр</h2>
                            <div class="__prod_price">400,000,000₮</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                        </div>
                    </div>
                    <div class="box_item prod_box box_vertical">
                        <div class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/project_img2.jpeg" />
                                </div>
                                <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                            </div>
                        </div>
                        <div class="prod_main">
                            <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                            <div class="__prod_price">280,000,000₮</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                        </div>
                    </div>
                    <div class="box_item prod_box box_vertical">
                        <div class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content"><img class="" src="images/project_img3.jpeg" /></div>
                                <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                            </div>
                        </div>
                        <div class="prod_main">
                            <h2 class="__prod_title_mini">130.5 м<sup>2</sup> өрөө байр</h2>
                            <div class="__prod_price">320,000,000₮</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                        </div>
                    </div>
                    <div class="box_item prod_box box_vertical">
                        <div class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/project_img2.jpeg" />
                                </div>
                                <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                            </div>
                        </div>
                        <div class="prod_main">
                            <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                            <div class="__prod_price">280,000,000₮</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                        </div>
                    </div>
                    <div class="box_item prod_box box_vertical">
                        <div class="prod_header">
                            <div class="__image_box">
                                <div class="__image_box_content">
                                <img class="" src="images/project_img1.jpeg" />
                                </div>
                                <div class="__image_box_content"><img class="" src="images/prod_img1.jpeg" /></div>
                            </div>
                        </div>
                        <div class="prod_main">
                            <h2 class="__prod_title_mini">90.5 м<sup>2</sup> 3 өрөө байр</h2>
                            <div class="__prod_price">280,000,000₮</div>
                            <p class="__prod_location">Улаанбаатар, Хан-Уул</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
